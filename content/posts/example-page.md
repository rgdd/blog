+++
title = "Example page"
date = "2023-11-09"
author = "John Doe"
cover = "/img/example.png"
tags = [ "demo", "cover" ]
keywords = [ "demo", "cover" ]
description = "Support front-page paragraph goes here. Defaults to the initial portion of the post if omitted."
draft = true
+++

The blog starts here below the (optional) cover image.  If there's no
description provided, the initial part of this text is used for that.

Note that there's no difference between, e.g., `#`, `##`, and `###`.  I.e., the
rendering in this template doesn't support section-hierarchies by default.

## Unordered list

  - First item
  - Second item

## Ordered list

  1. First item
  2. Second item

## Block quote

> This is a
> block quote.

## Code snippet: no syntax highlight

```
$ cd /path/to/directory
$ git init
```

(Can also be created without backticks and four-space intent instead.)

## Code snippet: C syntax highlighting

```c
#include <stdio.h>

int main()
{
    printf("hello, world\n");
    return 0;
}
```

## Code snippet: Go syntax highlighting

```go
// Package main is a hello-world program in Go
package main

import "fmt"

func main() {
    fmt.Printf("Hello, world\n")
}
```

## Code snippet: Bash syntax highlighting

```bash
#!/bin/bash

echo "Hello, world"
```

## Figure: plain-old markdown

![Example figurs](/img/example.png)

## Figure: shortcode with caption

{{< figure src="/img/example.png" position="center" style="border-radius: 0px;" caption="Some caption!" >}}

For additional options when using this short code, see:
https://github.com/panr/hugo-theme-terminal#built-in-shortcodes.

## Figure: shortcode, scale down figure to 67%

{{< image src="/img/example.png" position="center" style="border-radius: 0px; width: 67%;" >}}

For additional options when using this short code, see:
https://github.com/panr/hugo-theme-terminal#built-in-shortcodes.

## Video: locally recorded .webm file

{{< video-webm src="/video/example.webm" >}}

More options that can be enabled:

  - `autoplay="true"`
  - `loop="true"`

This shortcode is defined in `layouts/shortcodes/video-webm.html`, in case we
want to add more options or perhaps create another one for, e.g., !.webm format.
