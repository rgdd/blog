# README

Source code of a blog that is powered by [Hugo][] and the [terminal theme][].

[Hugo]: https://gohugo.io/
[terminal theme]: https://github.com/panr/hugo-theme-terminal

## Install

Clone this repository:

    $ git clone git@git.glasklar.is:rgdd/blog.git
    $ cd blog

Initialize he theme's submodule:

    $ git submodule update --init --recursive

Install [Hugo][]:

    # apt install hugo

## Run locally

In the top-most part of this repository:

    $ hugo serve

This command will block, hosting the website on http://localhost:1313.  Any
changes to the website's source will be rendered immediately.

(If you see any warnings about `languages.en...`, they can be ignored.)

## Update configuration and site metadata

See [hugo.toml](./hugo.toml) for the main configuration file.  For an example of
how to play with colors or remove/tweak some rectangles, refer to these commits:

    $ git show 1ccfd7d4c070dda4e534aab546bb46cb431ed127
    $ git show 138abbea4e0c054c7764d25561f8c2cf1b66e421

## Add content

The introduction that's viewed at the top of the frontpage is specified in
`content/_index.html`.  If no introduction is desired, delete this file.

New posts are added as .md files in the `content/posts` directory.  Posts that
have the date set in the future will not be rendered.  Posts with metadata
`draft: true` will also not be rendered.

To create a new post with all possible metadata keywords, use:

    $ hugo new posts/example.md
    Content "/home/rgdd/src/git.glasklar.is/rgdd/blog/content/posts/example.md" created

Or just copy a previous post and update that one accordingly!

Write the post in markdown below the `+++` header.

For some hints on some common metadata settings and syntax for figures, videos,
code, etc., see [example-page.md](./content/posts/example-page.md).  Note that
this page sets `draft = true`.  Therefore, it is not being rendered anywhere.

## Deploy to a remote server

Compile:

    $ hugo

This will create a directory named `public`.  Deploy it:

TODO: add rsync command once we have a remote server setup for this.

## Known template limitations

  - No built-in overview of tags that is nice, see `/tags` for an ugly overview.
  - No built-in search functionality.
  - No built-in overview rendering for all posts in dense form, i.e., the only
    overview for that right now is the main front-page or `/tags`.
  - Doesn't render `#`, `##`, `###` differently.
